#include <stdio.h>
#include <string.h>

char rotor1[] = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
char rotor2[] = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
char rotor3[] = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";

char findOriginal(char c, char* rotor) {
    for (int i = 0; i < 26; i++) {
        if (rotor[i] == c) {
            return 'A' + i; 
        }
    }
    return ' '; 
}


void decipher(char* message, int pseudoRandomNumber) {
    int len = strlen(message);

    for (int i = 0; i < len; i++) {
        char c = message[i];
        
        c = findOriginal(c, rotor3);
        c = findOriginal(c, rotor2);
        c = findOriginal(c, rotor1);
        
        c = ((c - 'A' - pseudoRandomNumber - i) % 26 + 26) % 26 + 'A';
        
        printf("%c", c);
    }

}


int main() {

    char message[] = "GEUDOPPSJPDDKDBJQBNYXGKKFYKTGMIWIHLGNDMGBEJOMKXICRECGMYHDT";

    int pseudoRandomNumber = 4;

    decipher(message, pseudoRandomNumber);

    return 0;
}
